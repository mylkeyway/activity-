// Minimal Example

import React, {Component} from 'react';
import {AppRegistry, View, Text, FlatList, TouchableHighlight} from 'react-native';

export default class FlatListExample extends Component{
    render(){
        return(
          <FlatList
          data={[{key: 'a'}, {key: 'b'}]}
          renderItem={({item}) => <Text>{item.key}</Text>}
        />
        );
    }
}

AppRegistry.registerComponent('FlatListExample', ()=> FlatListExample);